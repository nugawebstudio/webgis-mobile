/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {
  StackNavigator,
  createStackNavigator
} from 'react-navigation';


import Login from './src/components/Login';

import Dashboard from './src/pages/Dashboard';

export default class App extends Component {
  render() {
    return (
      <AppNavigator/>
    );
  }
}

const AppNavigator = createStackNavigator(
  {
    // LoadSpinner:{screen:LoadSpinner},
    
    Login: {screen: Login},
    Dashboard:{screen:Dashboard}


    
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none'
  }
);
